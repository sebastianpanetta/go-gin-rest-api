package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

// HomeGet funcion GET endpoint localhost:8080
func HomeGet(c *gin.Context) {
	c.JSON(200, gin.H{
		"mensaje" : "Home GET OK!",
	})
}

// HomePost funcion POST endpoint localhost:8080
func HomePost(c *gin.Context) {
	body := c.Request.Body
	value, err := ioutil.ReadAll(body)
	if err != nil {
		fmt.Println(err.Error())
	}
	c.JSON(200, gin.H{
		"mensaje" : string(value),
	})
}

// QueryStrings funcion para usar queryStrings en la URL localhost:8080/query
func QueryStrings(c *gin.Context) {
	nombre := c.Query("nombre")
	edad := c.Query("edad")
	c.JSON(200, gin.H{
		"Nombre" : nombre,
		"Edad" : edad,
	})
}

// PathParameters funcion para usar parametros, URL con formato localhost:8080/path/nombre/edad
func PathParameters(c *gin.Context) {
	nombre := c.Param("nombre")
	edad := c.Param("edad")
	c.JSON(200, gin.H{
		"Nombre" : nombre,
		"Edad" : edad,
	})
}

func main() {
	fmt.Println("Iniciando API REST GO...")
	r := gin.Default()
	r.GET("/", HomeGet)
	r.POST("/", HomePost)
	r.GET("/query", QueryStrings) // con queryStrings
	r.GET("/path/:nombre/:edad", PathParameters) 
	r.Run()
}